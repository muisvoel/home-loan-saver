import os
import datetime
from dateutil.relativedelta import relativedelta
import logging
import pandas as pd
import requests

import azure.functions as func

BASE_URL = 'https://openapi.investec.com'
CLIENT_ID = os.getenv('ClientID')
CLIENT_SECRET = os.getenv('ClientSecret')
TRANSACT_ACCOUNT_ID = os.getenv('TransactAccount')
HOME_LOAN_ACCOUNT_ID = os.getenv('HomeLoanAccount')

# The amount you don't want to go lower than in your transact account.
TRANSACT_ACCOUNT_LOWER_LIMIT = 10000.0

# The amount you want available in your transact account on the first of the month.
AMOUNT_FOR_FIRST = 30000.0

def get_access_token():
    """
    Returns access token which can be used to authenticate against Open Banking API.

    Returns:
        str: Token to be used to authenticate against Open Banking API.
    """
    bearer_token  = requests.post(url=f'{BASE_URL}/identity/v2/oauth2/token', 
                                  data = {'grant_type': 'client_credentials'}, 
                                  headers={'Content-Type': 'application/x-www-form-urlencoded'}, 
                                  auth=requests.auth.HTTPBasicAuth(CLIENT_ID, CLIENT_SECRET))

    bearer_token.raise_for_status()
    return bearer_token.json()['access_token']

def get_statement(access_token, accountid, from_date=None, to_date=None):
    """
    Packages account statement for accountid in a pandas dataframe between the date ranges specified.

    Args:
        access_token (str): Token to be used to authenticate against Open Banking API.
        accountid (str): Account ID
        from_date (str, optional): Lower date bound for statement in yyyy-mm-dd format. 
        to_date (str, optional): Upper date bound for statement in yyyy-mm-dd format. 
    
    Returns:
        DataFrame: Dataframe containing statement for the specified account and date range.
    """
    transactions  = requests.get(url=f'{BASE_URL}/za/pb/v1/accounts/{accountid}/transactions',
                                 params={'fromDate': from_date, 'toDate': to_date}, 
                                 headers={'Authorization': f'Bearer {access_token}'})

    transactions.raise_for_status()
    statement = transactions.json()['data']['transactions']
    
    statement_df = pd.DataFrame(statement)
    statement_df['transactionDate'] = pd.to_datetime(statement_df['transactionDate'])
    return statement_df

def large_deposit_checker(df, date, transaction_desc_regex=['.*salary.*']):
    """
    Checks whether there are any deposits for the input date matching the provided regular expression.

    Args:
        df (DataFrame): DataFrame containing statement.
        date (str): Date in yyyy-mm-dd format.
        transaction_desc_regex (list[str], optional): Regular expressions to limit the type of large deposits to look for. 
                                                      Defaults to '.*salary.*', which assumed your large deposit of interest 
                                                      contains the keyword 'salary'. This can be changed or removed as desired.

    Returns:
        bool: True is large depost matching date and regular expression exists.
    """
    
    large_deposit_df = df.query('transactionDate==@date & type=="CREDIT"')
    
    match_regex = False
    for regex in transaction_desc_regex:
        match_regex = True if any(large_deposit_df.description.str.lower().str.fullmatch(regex)) else False
    return match_regex
    
def available_balance(access_token, accountid):
    """
    Returns the available balance for the account specified by accountid

    Args:
        access_token (str): Token to be used to authenticate against Open Banking API.
        accountid (str): The account ID.

    Returns:
        [type]: The availabe balance for the account.
    """
    balance  = requests.get(url=f'{BASE_URL}/za/pb/v1/accounts/{accountid}/balance',
                            headers={'Authorization': f'Bearer {access_token}'})
    balance.raise_for_status()
    
    balance = balance.json()
    return balance['data']['availableBalance']

def transfer(amount, from_account, from_account_reference, to_account, to_account_reference, access_token):
    """
    Helper to transfer money between the two specified accounts.

    Args:
        amount (float): Amount of money (ZAR) to transfer.
        from_account (str): Account ID to move money from.
        from_account_reference (str): Reference for account money is being moved from.
        to_account (str): Account ID for where the money is being sent. 
        to_account_reference (str): Reference for the account where the money is being sent.
        access_token (str): Token to be used to authenticate against Open Banking API.
    """

    body = {"AccountId": from_account,
            "TransferList": [
                    {
                        "BeneficiaryAccountId": to_account,
                        "Amount": str(amount),
                        "MyReference": from_account_reference,
                        "TheirReference": to_account_reference
                    }
                ]
            }

    requests.post(url=f'{BASE_URL}/za/pb/v1/accounts/transfermultiple',
                  json=body, 
                  headers={'Content-Type': 'application/json',
                           'Authorization': f'Bearer {access_token}'})
    

def main(mytimer: func.TimerRequest) -> None:

    logging.info('Starting HomeLoanSaver at %s',  datetime.datetime.now())

    today = datetime.date.today()
    try:
        access_token = get_access_token()
        logging.info('Access token acquired.')
    except requests.exceptions.HTTPError:
        logging.info('Unable to obtain access token.')
        return 0

    try:
        statement_df = get_statement(access_token, TRANSACT_ACCOUNT_ID)
        logging.info('Statement acquired.')
    except requests.exceptions.HTTPError:
        logging.info('Unable to get statement.')
        return 0

    large_deposit_exists = large_deposit_checker(statement_df, today.strftime('%Y-%m-%d'))

    try:
        transact_balance = available_balance(access_token, TRANSACT_ACCOUNT_ID)
        logging.info('Transact available balance acquired.')
    except requests.exceptions.HTTPError:
        logging.info('Unable to obtain Transact balance.')
        return 0

    try:
        hl_balance = available_balance(access_token, HOME_LOAN_ACCOUNT_ID)
        logging.info('Home Loan available balance acquired.')
    except requests.exceptions.HTTPError:
        logging.info('Unable to obtain Home Loan balance.')
        return 0

    # If salary has been paid in, transfer money to HL account
    try:
        if large_deposit_exists & (transact_balance > TRANSACT_ACCOUNT_LOWER_LIMIT):
            amount_to_transfer = transact_balance-TRANSACT_ACCOUNT_LOWER_LIMIT
            transfer(amount_to_transfer, 
                    from_account=TRANSACT_ACCOUNT_ID,
                    from_account_reference='Saving on Home Loan interest',
                    to_account=HOME_LOAN_ACCOUNT_ID,
                    to_account_reference='Temporary salary depost',
                    access_token=access_token)
            logging.info('Successfully transferred R %s to Home Loan account.', amount_to_transfer)
    except requests.exceptions.HTTPError:
        logging.info('Unable to transfer money to Home Loan Account.')
        return 0

    # If it's the last day of the month, top up transact account
    try:
        if (today == (today + relativedelta(day=31))) & (transact_balance < AMOUNT_FOR_FIRST):
            amount_to_transfer = AMOUNT_FOR_FIRST - transact_balance
            if hl_balance > amount_to_transfer:
                transfer(amount_to_transfer, 
                        from_account=HOME_LOAN_ACCOUNT_ID,
                        from_account_reference='Money for the month',
                        to_account=TRANSACT_ACCOUNT_ID,
                        to_account_reference='Money for the month',
                        access_token=access_token)
                logging.info('Successfully transferred R %s to to Transact Account.', amount_to_transfer)
            else:
                logging.info('Not enough money in HL account to transfer to Transact Account.')
    except requests.exceptions.HTTPError:
        logging.info('Unable to transfer money to Transact Account.')
        return 0

    logging.info('Python timer trigger function completed successfully.')
